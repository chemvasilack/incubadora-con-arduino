**Incubadora de Huevos**

Este proyecto consiste en construir una incubadora para huevos de gallinas. La cual estara controlada por Arduino UNO. 

**Objetivo**

El objetivo principal es construir una incubadora de huevos y que arduino se encarge de mantener la temperatura adecuada para que al pasar los días necesarios puedan nacer los pollitos con la menor tasa de mortalidad.
 
**Parametros a Controlar**

- Humedad y Temperatura : La temperatura y humedad estara controlada mediante un sensor DHT22
- Ventilación : La ventilacion estara controlada por el uso de ventiladores ("_Especificar_")

los datos de temperatura, humedad se podran visualizar mediante un Display LCD.


**Componentes necesarios:**

- Modulo Relay.
- Arduino Uno.
- Un sensor temperatura DHT22.
- LCD 16 X 2.
- Calefactor (_"Especificar Calefactor"_)
- Ventilador (_"Especificar Calefactor"_)
