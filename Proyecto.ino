// importo librerias

#include <DHT_U.h>
#include <DHT.h>
#include <LiquidCrystal.h>
#define DHTTYPE DHT22

//creo las variables con las que trabajare
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
int SENSOR = 8;
int TEMPERATURA;
int HUMEDAD;
int RELE = 9;

DHT dht(SENSOR, DHT22);


void setup() {

  pinMode(RELE,OUTPUT);

  //inicio DHT y el LCD

  dht.begin();

  lcd.begin(16, 2);

  //imprimo mensaje de prueba

  lcd.print("Hola Ema");

  delay(1000);

}

void loop() {

  //a las variables TEMPERATURA y HUMEDAD le asigno los valores que detecta el
  // SENSOR DHT

  TEMPERATURA = dht.readTemperature();
  HUMEDAD = dht.readHumidity();

  lcd.clear();//Elimina todos los simbolos del LCD
  lcd.setCursor(0,0);//Posiciona la primera letra despues del segmento 5 en linea 1
  lcd.print("Humedad: ");
  lcd.setCursor(8,0);
  lcd.print(HUMEDAD); //muestra la HUMEDAD
  lcd.print("%");

  lcd.setCursor(0,1);
  lcd.print("Temperatura: ");
  lcd.setCursor(12,1);
  lcd.print(TEMPERATURA);//muestra la temperatura
  lcd.print("C");
  delay (2500);

  if(TEMPERATURA <= 25){ //Especificar temperatura correcta

   digitalWrite(RELE, HIGH);
   lcd.clear();
   lcd.setCursor(0,0);
   lcd.print("CALENTADOR ON");
   delay(2000);

  }

  if(TEMPERATURA > 25){ //Especificar temperatura correcta

   digitalWrite(RELE, LOW);
   lcd.clear();
   lcd.setCursor(0,0);
   lcd.print("CALENTADOR OFF");
   delay(2000);
  }

}
